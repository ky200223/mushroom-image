from django.test import TestCase, Client
from django.conf import settings


class UnitTest(TestCase):
    def test_upload_image(self):
        with open(settings.BASE_DIR + 'README.md', 'rb') as f:
            response = Client().post(path='/images/upload', data={'image': f})
            assert response.status_code is 200

    def test_upload_image_without_filename(self):
        # need self test
        pass