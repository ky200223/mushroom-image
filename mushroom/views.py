import os
import binascii
import time
import logging

from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from django.conf import settings


logger = logging.getLogger(__name__)


@require_http_methods("POST")
def upload(req):
    def gen_image_name(file):
        _, dot_extension = os.path.splitext(file.name)
        if not dot_extension:
            dot_extension = '.' + file.content_type.split('/')[-1]

        sec = str(int(time.time()))
        random_string = binascii.b2a_hex(os.urandom(15)).decode('utf-8')
        return sec + '_' + random_string + dot_extension

    def save_to_disk(file, name, path):
        with open(os.path.join(path, name), 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)

    try:
        image = req.FILES['image']
        image_name = gen_image_name(image)
        save_to_disk(file=image, name=image_name, path=settings.IMAGE_DIR)
    except Exception as e:
        return HttpResponse(status=406, content_type='text/plain', content=e)
    return HttpResponse(status=200, content_type='text/plain', content=image_name)