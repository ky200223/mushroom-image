Mushroom image server
======================

Run
----

    (pyvenv)$ export DJANGO_SETTINGS_MODULE=MushroomImageServer.settings
    (pyvenv)$ export IMAGE_DIR=...
    (pyvenv)$ python manage.py runserver


APIs
-----
* POST /images/upload


### /images/upload (POST)
request
    Content-Type: multipart/form-data
    ---------------------------------boundary
    name: image, filename: image.png, Content-Type: image/png
    ...data...
    
response 200
    Content-Type: 'text/plain'
    body: image_name.jpg